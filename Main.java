import java.awt.FlowLayout;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

public class Main {
  public static void main(String[] args) {
    // Load the native library.
    System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	BufferedImage img = null;
	
	try {
		img = ImageIO.read(new File("/Users/rune/Downloads/paul.jpeg"));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	final Mat mat = bufferedImageToMat(img);
    JFrame frame=new JFrame();
    frame.setLayout(new FlowLayout());        
    frame.setSize(img.getWidth(null), img.getHeight(null));     
    JLabel lbl=new JLabel();
    ImageIcon icon=new ImageIcon(img);
    lbl.setIcon(icon);
    lbl.addMouseListener(new MouseListener() {
		
		@Override
		public void mouseReleased(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mousePressed(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseExited(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseEntered(MouseEvent e) {
			// TODO Auto-generated method stub
			
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			int posX = e.getX();
			int posY = e.getY();
			
			System.out.println("X:"+posX+" Y:"+posY);
			Mat matcp = mat.clone();
			BufferedImage img2 = Mat2BufferedImage(displayImage(matcp,posX,posY));
			ImageIcon icon=new ImageIcon(img2);
		    lbl.setIcon(icon);
			//mat.get(, col)
		}
	});
    frame.add(lbl);
    frame.setVisible(true);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    
  }
  
  public static Mat displayImage(Mat mat,int x,int y)
    {   
		Mat hsv = new Mat();
		Mat imgThresholded = new Mat();
		Boolean[][] select = new Boolean[mat.width()/3][mat.height()];
	    Imgproc.cvtColor(mat, hsv, Imgproc.COLOR_RGB2HSV);
	    int offset = 15;
	    int iLowV = Math.max((int)(hsv.get(y, (x))[2]-offset) ,0);//175;//240 * (255/360);
		int iLowS = Math.max((int)(hsv.get(y, (x))[1]-offset) ,0);// * (255/100);
		int iLowH = Math.max((int)(hsv.get(y, (x))[0]-offset) ,0);// * (255/100);
		
		int iHighV = Math.min((int)(hsv.get(y, (x))[2]+offset) ,255);// * (255/360);
		int iHighS = Math.min((int)(hsv.get(y, (x))[1]+offset) ,255);// * (255/100);
		int iHighH = Math.min((int)(hsv.get(y, (x))[0]+offset) ,255);// * (255/100);
		
		System.out.println(mat.get(y, (x))[0] +", "+mat.get(y, (x))[1]+", "+mat.get(y, (x))[2]);
		//System.out.println(iLowR+", "+ iLowG+", "+iLowB+" : "+iHighR+", "+iHighG+", "+iHighB);
		Core.inRange(hsv,  new Scalar(iLowH, iLowS, iLowV),new Scalar(iHighH, iHighS, iHighV), imgThresholded); //Threshold the image
		Imgproc.dilate( imgThresholded, imgThresholded, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5)) ); 
		//copy to Array
		for(int h = 0; h < imgThresholded.height(); h++){ // Zeilen
			for(int w = 0; w < imgThresholded.width(); w+=3){
				select[w/3][h] = (((imgThresholded.get(h,w)[0] == 255)&&(imgThresholded.get(h,w+1)[0] == 255)&&(imgThresholded.get(h,w+2)[0] == 255)) ? true : false);
				if((imgThresholded.get(h,w)[0] == 255)&&(imgThresholded.get(h,w+1)[0] == 255)&&(imgThresholded.get(h,w+2)[0] == 255)){hsv.put(h, w, new double[]{0,0,0});hsv.put(h, w+1, new double[]{0,0,0});hsv.put(h, w+2, new double[]{0,0,0});}//hsv.put(h, w+1, new double[]{200.0,200.0,200.0});hsv.put(h, w+2, new double[]{200.0,200.0,200.0});}
				//mat.put(w+1, h, new double[]{200.0});
				//mat.put(w+2, h, new double[]{200.0});
				System.out.print((imgThresholded.get(h,w)[0] == 255 ? "1": "0"));
			}
			
			System.out.println(";");
		}
		hsv.put(y, x, new double[]{255.0,0,0});
		  //morphological opening (remove small objects from the foreground)
		   //Imgproc.erode(imgThresholded, imgThresholded, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5)) );
		   //Imgproc. dilate( imgThresholded, imgThresholded, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5)) ); 

		   //morphological closing (fill small holes in the foreground)
		   
		   //Imgproc.erode(imgThresholded, imgThresholded, Imgproc.getStructuringElement(Imgproc.MORPH_ELLIPSE, new Size(5, 5)) );
		/*try{
		    PrintWriter writer = new PrintWriter("the-file-name.txt", "UTF-8");
		    writer.println("The first line");
		    writer.println(imgThresholded.dump());
		    writer.close();
		} catch (Exception e) {*/
		   // do something
		//}
		Imgproc.cvtColor(hsv, mat, Imgproc.COLOR_HSV2RGB);
		return mat;

    }
  public static Mat bufferedImageToMat(BufferedImage bi) {
	  Mat mat = new Mat(bi.getHeight(), bi.getWidth(), CvType.CV_8UC3);
	  byte[] data = ((DataBufferByte) bi.getRaster().getDataBuffer()).getData();
	  mat.put(0, 0, data);
	  return mat;
	}
  
  public static BufferedImage Mat2BufferedImage(Mat m){
	//source: http://answers.opencv.org/question/10344/opencv-java-load-image-to-gui/
	//Fastest code
	//The output can be assigned either to a BufferedImage or to an Image
	  
	  int type = BufferedImage.TYPE_BYTE_GRAY;
	  if ( m.channels() > 1 ) {
	      type = BufferedImage.TYPE_3BYTE_BGR;
	  }
	  int bufferSize = m.channels()*m.cols()*m.rows();
	  byte [] b = new byte[bufferSize];
	  m.get(0,0,b); // get all the pixels
	  BufferedImage image = new BufferedImage(m.cols(),m.rows(), type);
	  final byte[] targetPixels = ((DataBufferByte) image.getRaster().getDataBuffer()).getData();
	  System.arraycopy(b, 0, targetPixels, 0, b.length);  
	  return image;
  }
  
}